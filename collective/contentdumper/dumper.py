import base64
from Acquisition import aq_inner
from DateTime import DateTime
from five import grok
from logging import getLogger
from plone import api
from plone.app.textfield.value import RichTextValue
from plone.namedfile.interfaces import INamedBlobFileField
from plone.namedfile.interfaces import INamedBlobImageField
from plone.app.textfield.interfaces import IRichText
from plone.dexterity.interfaces import IDexterityContainer
from zope.interface import Interface
from zope.schema.interfaces import IField
from zope.schema.interfaces import IDatetime
from zope.schema.interfaces import IList


log = getLogger('Dumper')


class IFieldDumper(Interface):
    """ General field dumper. """


class FieldDumper(grok.Adapter):
    grok.provides(IFieldDumper)
    grok.context(IField)

    def __init__(self, field):
        self.field = field
        self.fname = field.getName()

    def info(self, value):
        log.info('%s (%s) - %s' % \
            (self.fname, self.field.__class__.__name__, value))

    def dump(self, obj):
        value = getattr(obj, self.fname, (self.field.default or u''))
        self.info(value)
        return value


class RichTextDumper(FieldDumper):
    grok.provides(IFieldDumper)
    grok.context(IRichText)

    def dump(self, obj):
        value = getattr(obj, self.fname, self.field.default)
        if isinstance(value, str):
            value = {
                'type': 'str',
                'raw': value
            }
        elif isinstance(value, unicode):
            value = {
                'type': 'unicode',
                'raw': value
            }
        elif isinstance(value, RichTextValue):
            value = value and {
                'type': 'RichTextValue',
                'raw': value.raw,
                'mimeType': value.mimeType,
                'outputMimeType': value.outputMimeType,
                'encoding': value.encoding
            } or None
        self.info(value)
        return value


class DatetimeDumper(FieldDumper):
    grok.provides(IFieldDumper)
    grok.context(IDatetime)

    def dump(self, obj):
        value = getattr(obj, self.fname, (self.field.default or None))
        # may be a method e.g.: expires
        if callable(value):
            value = value()
        # dump as timestamp
        if isinstance(value, DateTime):
            value = int(value)
        elif isinstance(value, int):
            value = value
        else:
            raise NotImplemented('Unsupported datetime type.')
        self.info(value)
        return value


class ListDumper(FieldDumper):
    grok.provides(IFieldDumper)
    grok.context(IList)

    def dump(self, obj):
        value = getattr(obj, self.fname, (self.field.default or None))
        if value:
            # check for uids and transform them to paths relative to portal
            result = []
            utool = api.portal.get_tool('portal_url')
            for record in value:
                obj = api.content.get(UID=record)
                if obj is not None:
                    path = '/'.join(utool.getRelativeContentPath(obj))
                    result.append({
                        'type': 'UID',
                        'value': path
                        })
                else:
                    result.append({
                        'type': 'str',
                        'value': record
                        })
            value = result
        self.info(value)
        return value


class BlobImageDumper(FieldDumper):
    grok.provides(IFieldDumper)
    grok.context(INamedBlobImageField)

    def dump(self, obj):
        value = getattr(obj, self.fname, self.field.default)
        value = value and {
            'filename': value.filename,
            'contentType': value.contentType,
            'data': base64.b64encode(value.data)
            } or None
        self.info(value)
        return value


class BlobFileDumper(FieldDumper):
    grok.provides(IFieldDumper)
    grok.context(INamedBlobFileField)

    def dump(self, obj):
        value = getattr(obj, self.fname, self.field.default)
        value = value and value.raw or u''
        self.info(value)
        return value


class IContentDumper(Interface):
    """ Component for dumping dexterity based content. """


class ContentDumper(grok.Adapter):
    grok.provides(IContentDumper)
    grok.context(IDexterityContainer)

    def __init__(self, context):
        self.context = context

    def dump(self, fields, portal_type, additional=[]):
        context = aq_inner(self.context)
        catalog = api.portal.get_tool(name='portal_catalog')
        path = '/'.join(context.getPhysicalPath())
        brains = catalog(
            portal_type=portal_type,
            path={'query': path, 'depth': 1000}
            )

        result = []
        for brain in brains:
            obj = brain.getObject()

            # core attributes
            item = {
                '_id': obj.id,
                '_path': '/'.join(obj.getPhysicalPath()[:-1]),
                '_portal_type': obj.portal_type,
                '_review_state': api.content.get_state(obj),
                '_layout': obj.getProperty('default_page'),
            }

            # schemata fields
            props = []
            for field in fields:
                fname = field['id']
                ftype = field['type']
                fvalue = IFieldDumper(field['instance']).dump(obj)
                try:
                    import cjson
                    cjson.encode(fvalue)
                except:
                    raise ValueError(
                        'Not jsonable - %s (%s): %s' % (fname, ftype, fvalue))
                field = {
                    'name': fname,
                    'type': ftype,
                    'value': fvalue
                }
                props.append(field)

            # additional attributes
            props.extend(additional)

            item['_fields'] = props

            result.append(item)
        return result
