import base64
from Acquisition import aq_inner
from five import grok
from logging import getLogger
from plone import api
from plone.app.textfield.value import RichTextValue
from plone.dexterity.interfaces import IDexterityContainer
from plone.namedfile import NamedBlobImage
from plone.namedfile import NamedBlobFile
from Products.CMFPlone.utils import safe_unicode
from zope.interface import Interface

log = getLogger('Loader')


class IContentLoader(Interface):
    """ Component for loading dexterity based content. """


class ContentLoader(grok.Adapter):
    grok.provides(IContentLoader)
    grok.context(IDexterityContainer)

    def __init__(self, context):
        self.context = context

    def load(self, source):
        context = aq_inner(self.context)
        source = sorted(source, key=lambda x: x['_path'])

        created = 0
        updated = 0

        for item in source:
            item_id = item['_id']
            #item_path = item['_path']

            # check if content exists already
            if item_id not in context:
                # create content
                obj = api.content.create(
                    context, item['_portal_type'], item['_id'])
                created += 1
                log.info('Created: %s' % '/'.join(obj.getPhysicalPath()))
            else:
                obj = context[item_id]
                updated += 1
                log.info('Updating: %s' % '/'.join(obj.getPhysicalPath()))

            # update schemata fields
            for field in item['_fields']:
                fname = field['name']
                ftype = field['type']
                fvalue = field['value']

                if ftype == 'Int':
                    setattr(obj, fname, int(fvalue))

                elif ftype == 'Float':
                    setattr(obj, fname, float(fvalue))

                elif ftype == 'Tuple':
                    if fvalue:
                        if isinstance(fvalue, list):
                            fvalue = tuple(fvalue)
                        elif isinstance(fvalue, str):
                            raise ValueError("Don't know how to load tuple attribute with value: %s" % fvalue)
                        elif isinstance(fvalue, tuple):
                            pass
                        else:
                            raise NotImplemented("Don't know how to load value: %s into tuple." % fvalue)

                elif ftype == 'List':
                    if fvalue:
                        portal = api.portal.get()
                        result = []
                        for record in fvalue:
                            rtype = record['type']
                            rvalue = record['value']
                            if rtype == 'UID':
                                target = portal.restrictedTraverse(rvalue)
                                if target is not None:
                                    uid = api.content.get_uuid(obj=target)
                                    result.append(uid)
                            elif rtype == 'str':
                                result.append(rvalue)
                            else:
                                raise NotImplemented("Unknown list record type.")
                        setattr(obj, fname, result)

                elif ftype == 'RichText':
                    if fvalue:
                        ftype = fvalue['type']
                        if ftype == 'str':
                            fvalue = str(fvalue['raw'])
                        elif ftype == 'unicode':
                            fvalue = safe_unicode(fvalue['raw'])
                        elif ftype == 'RichTextValue':
                            fvalue = RichTextValue(
                                raw=fvalue['raw'],
                                mimeType=fvalue['mimeType'],
                                outputMimeType=fvalue['outputMimeType'],
                                encoding=fvalue['encoding']
                                )
                        setattr(obj, fname, fvalue)

                elif ftype == 'NamedBlobImage':
                    if fvalue:
                        fvalue['filename'] = safe_unicode(fvalue['filename'])
                        fvalue['data'] = base64.b64decode(fvalue['data'])
                        fvalue = NamedBlobImage(**fvalue)
                        setattr(obj, fname, fvalue)

                elif ftype == 'NamedBlobFile':
                    if fvalue:
                        # TODO handle blob files
                        raise NotImplemented("Don't know how to handle blob files yet.")

                elif ftype == 'NamedImage':
                    if fvalue:
                        # TODO handle images
                        raise NotImplemented("Don't know how to handle images yet.")

                elif ftype == 'NamedFile':
                    if fvalue:
                        # TODO handle files
                        raise NotImplemented("Don't know how to handle files yet.")

                else:
                    setattr(obj, fname, fvalue)

            # TODO: handle display views
            # TODO: handle review state (optional)

            obj.reindexObject()

        return (created, updated)
