import cjson
from Acquisition import aq_inner
from collective.contentdumper.i18n import _
from collective.contentdumper.dumper import IContentDumper
from collective.contentdumper.loader import IContentLoader
from five import grok
from logging import getLogger
from plone import api
from plone.app.vocabularies.types import ReallyUserFriendlyTypesVocabularyFactory
from plone.dexterity.interfaces import IDexterityContainer
#from plone.uuid.interfaces import IUUID
from Products.statusmessages.interfaces import IStatusMessage
from zope.dottedname.resolve import resolve
from zope.i18nmessageid.message import Message

grok.templatedir('templates')
log = getLogger(__name__)


class DumperView(grok.View):
    grok.context(IDexterityContainer)
    grok.name('content-dumper')
    grok.template('dumper')
    grok.require('cmf.ManagePortal')

    @property
    def additional_types(self):
        return [
            {'id': 'TextLine', 'title': _(u'String')},
            {'id': 'Int', 'title': _(u'Integer')},
            {'id': 'Float', 'title': _(u'Float')},
            {'id': 'Tuple', 'title': _(u'Tuple')},
            {'id': 'List', 'title': _(u'List')},
        ]

    def _listFields(self, iface):
        result = {
            'id': iface.__identifier__,
            'title': iface.getName(),
            'fields': []
            }
        for fid in iface.names():
            field = iface.get(fid)
            if isinstance(field, unicode):
                ftitle = field.title
            elif isinstance(field, Message):
                ftitle = field.title.default
            else:
                ftitle = field.title
            result['fields'].append({
                'id': fid,
                'title': ftitle,
                'type': field.__class__.__name__,
                'instance': field
                })
        return result

    def _listBehaviors(self, tinfo):
        result = []
        for iname in tinfo.behaviors:
            iface = resolve(iname)
            result.append(self._listFields(iface))
        return result

    def _compileAdditional(self, tp, value):
        types = [x['id'] for x in self.additional_types]

        if tp == types[0]:
            # string
            return value
        elif tp == types[1]:
            # integer
            return int(value)
        elif tp == types[2]:
            # float
            return float(value)
        elif tp == types[3]:
            # tuple
            return tuple(value.split(','))
        elif tp == types[4]:
            return value.split(',')

    def __call__(self, **kwargs):
        super(DumperView, self).update()
        context = aq_inner(self.context)
        types_vocab = ReallyUserFriendlyTypesVocabularyFactory(context)
        self.portal_types = [tp for tp in types_vocab]

        f = self.request.form
        if 'submitted' in f:

            if 'dump-go' in f:
                msgs = IStatusMessage(self.request)
                portal_type = f.get('portal_type')
                if not portal_type:
                    msgs.addStatusMessage(_(u'Please select content type to dump.'), 'warning')
                else:
                    types_tool = api.portal.get_tool(name='portal_types')
                    tinfo = types_tool.getTypeInfo(portal_type)
                    try:
                        schema = tinfo.lookupSchema()
                    except AttributeError:
                        schema = None

                    if not schema:
                        msgs.addStatusMessage(_(u'Only dexterity based content is supported.'), 'warning')
                    else:
                        self.schema = self._listFields(schema)
                        self.behaviors = self._listBehaviors(tinfo)

                        field_list = [field for field in self.schema['fields']]
                        for schema in self.behaviors:
                            field_list.extend([field for field in schema['fields']])

                        additional = []
                        add_name = f.get('add_name', [])
                        add_type = f.get('add_type', [])
                        add_value = f.get('add_value', [])
                        step = 0
                        for aname in add_name:
                            avalue = add_value[step]
                            if aname and avalue:
                                atype = add_type[step]
                                try:
                                    additional.append({
                                        'name': aname,
                                        'type': atype,
                                        'value': self._compileAdditional(atype, avalue)
                                        })
                                except:
                                    msgs.addStatusMessage(_(u'Failed to compile additional value: $value',
                                        mapping={'value': avalue}))
                                    additional = 'ERROR'
                                    break
                            step += 1

                        if additional != 'ERROR':
                            dumper = IContentDumper(context)
                            dump = cjson.encode(dumper.dump(field_list, portal_type, additional))
                            response = self.request.response
                            response.setHeader("Content-type", "application/json; charset=utf-8")
                            response.setHeader("Content-Transfer-Encoding", "8bit")

                            cd = 'attachment; filename=%s_%s.json' % (context.id, portal_type)
                            response.setHeader('Content-Disposition', cd)
                            return dump

            elif 'load-go' in f:
                source_file = f.get('source_file')
                if not source_file:
                    IStatusMessage(self.request).addStatusMessage(_(u'Please select a source file.'), 'warning')
                else:
                    loader = IContentLoader(context)
                    result = loader.load(cjson.decode(source_file.read()))
                    IStatusMessage(self.request).addStatusMessage(
                        _(u'Content loading done - created: ${created}, updated: ${updated}.',
                            mapping={'created': result[0], 'updated': result[1]}), 'info')

        return super(DumperView, self).__call__(**kwargs)
