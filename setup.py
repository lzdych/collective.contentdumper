from setuptools import setup, find_packages
import os

version = '0.1dev'

setup(name='collective.contentdumper',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        ],
      keywords='',
      author='Lukas Zdych',
      author_email='lukas.zdych@gmail.com',
      url='http://svn.plone.org/svn/collective/',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['collective'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'python-cjson',
          'five.grok',
          'plone.api',
      ],
      entry_points="""
      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
